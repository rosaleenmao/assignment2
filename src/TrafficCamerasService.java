import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class TrafficCamerasService {
	
	public int findCameras(String parameter, String value, String value2) throws JAXBException, MalformedURLException, IOException
	{
		int numberOfCameras = 0;
	    JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	     
	    Response cameraNodes = (Response) jaxbUnmarshaller.unmarshal( 
	    		new URL("http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml").openStream());
	    if (parameter.equals("both")) {
	    	for(Row camera : cameraNodes.getRowList()) {
		        if(camera.getCameraStatus().equalsIgnoreCase(value) && camera.getIpCommStatus().equalsIgnoreCase(value2)) numberOfCameras++;
		    }
	    }
	    else if (parameter.equals("camera_status")) {
	    	for(Row camera : cameraNodes.getRowList()) {
		        if(camera.getCameraStatus().equalsIgnoreCase(value)) numberOfCameras++;
		    }
	    } else {
	    	for(Row camera : cameraNodes.getRowList()) {
	    		if(camera.getIpCommStatus().equalsIgnoreCase(value)) numberOfCameras++;
		    }
	    }
	    return numberOfCameras;
	}
}