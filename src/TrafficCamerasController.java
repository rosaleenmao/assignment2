import java.io.IOException;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBException;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TrafficCamerasController {

	private TrafficCamerasService trafficCamerasService;

	public TrafficCamerasController() {
	}
	
	public TrafficCamerasController(TrafficCamerasService service) {
		this.trafficCamerasService = service;
	}
	
	@ResponseBody
    @RequestMapping(value = "/")
    public String initialize()
    {
        return "Please enter a valid parameter.";
    }

	@ResponseBody
    @RequestMapping(value = "/trafficcameras", method=RequestMethod.GET)
    public String findCameras(@RequestParam(value = "ip_comm_status", required=false) String ip_comm_status, @RequestParam(value = "camera_status", required=false) String camera_status) throws MalformedURLException, JAXBException, IOException
    //I learned about optional parameters from: https://stackoverflow.com/questions/22373696/requestparam-in-spring-mvc-handling-optional-parameters
	{
		String ret = "";
		if (camera_status == null && ip_comm_status==null) {
			ret = "Please enter a valid parameter.";
			
		} else if (camera_status == null) {
			//check if parameter is valid
			String isValid = isValid("ip_comm_status", ip_comm_status);
			if (!(isValid.equals("valid"))) return isValid;
			
			//if valid, find cameras
			ret = "Number of cameras with ip_comm_status '" + ip_comm_status + "': "
					+ trafficCamerasService.findCameras("ip_comm_status", ip_comm_status, "") + "\n";
			
		} else if (ip_comm_status == null) {
			//check if parameter is valid
			String isValid = isValid("camera_status", camera_status);
			if (!(isValid.equals("valid"))) return isValid;
			
			//if valid, find cameras
			ret = "Number of cameras with camera_status '" + camera_status + "': "
					+ trafficCamerasService.findCameras("camera_status", camera_status, "") + "\n";
			
		} else {
			//check if parameters are valid
			String isValid = isValid("ip_comm_status", ip_comm_status);
			if (!(isValid.equals("valid"))) return isValid;
			isValid = isValid("camera_status", camera_status);
			if (!(isValid.equals("valid"))) return isValid;
			
			//if valid, find cameras
			ret = "Number of cameras with camera_status '" + camera_status + "', ip_comm_status '" + ip_comm_status + "': "
					+ trafficCamerasService.findCameras("both", camera_status, ip_comm_status) + "\n"; 
		}
		return ret;
    }
	
	private String isValid(String parameter, String value) {
		String ret = "valid";
		if (parameter.equals("ip_comm_status")) {
			if (!value.equalsIgnoreCase("no communication") &&
					!value.equalsIgnoreCase("offline") &&
					!value.equalsIgnoreCase("online")) {
				ret = "Value '" + value + "' not a valid value for ip_comm_status.\n";
			}
		} else if (!value.equalsIgnoreCase("desired") &&
					!value.equalsIgnoreCase("removed") &&
					!value.equalsIgnoreCase("turned_on") &&
					!value.equalsIgnoreCase("void")) {
			ret = "Value '" + value + "' not a valid value for camera_status.\n";
		}
		return ret;
	}
}